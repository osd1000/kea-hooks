// pkt4_receive.cc
#include <hooks/hooks.h>
#include <dhcp/pkt4.h>

using namespace isc::dhcp;
using namespace isc::hooks;
using namespace std;
extern "C" {

// This callout is called at the "pkt4_receive" hook.
  int pkt4_receive(CalloutHandle& handle) {
    Pkt4Ptr query4_ptr;
    handle.getArgument("query4", query4_ptr);
    if (query4_ptr->getType() == isc::dhcp::DHCPDECLINE) {
      // it's a DECLINE, drop it on the floor
      handle.setStatus(CalloutHandle::NEXT_STEP_DROP);
    }
    return (0);
  };
}
