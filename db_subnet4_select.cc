// subnet4_select.cc
#include <hooks/hooks.h>
#include <dhcp/pkt4.h>
#include <dhcpsrv/subnet.h>
#include <dhcpsrv/base_host_data_source.h>
#include <dhcpsrv/host_mgr.h>
#include <iostream>

// Select subnet based on reservation hwaddr

using namespace isc::dhcp;
using namespace isc::hooks;
using namespace std;
extern "C" {

  int subnet4_select(CalloutHandle& handle) {
    Pkt4Ptr pkt4_ptr;
    const Host::IdentifierType &idtype_ref = Host::IDENT_HWADDR;
    HWAddrPtr hwaddr_ptr;
    Subnet4Ptr subnet4_ptr;
    const Subnet4Collection *subnets;
    HostDataSourcePtr hds_ptr;
    ConstHostCollection chs;
    ConstHostPtr f;
    int subnet_id;

    handle.getArgument("query4", pkt4_ptr);

    hwaddr_ptr=pkt4_ptr->getHWAddr();

    handle.getArgument("subnet4", subnet4_ptr);

    hds_ptr=HostMgr::instance().getHostDataSource();

    chs=hds_ptr->getAll(idtype_ref, hwaddr_ptr->hwaddr_.data(), 6);

    if (chs.size() > 0) {
      // get ipv4_subnet_id
      f=chs.front();
      subnet_id=f->getIPv4SubnetID();

      // iterate through collection of subnets looking foor ours as we
      // need to emit a pointer.
      handle.getArgument("subnet4collection", subnets);

      for (int i = 0; i < subnets->size();  ++i) {
      	Subnet4Ptr new_subnet = (*subnets)[i];
      	if (new_subnet->getID() == subnet_id) {
      	  handle.setArgument("subnet4", new_subnet);
      	  break;
      	}
      }

    }
    return (0);
  };
}
