CXX=g++
CXXFLAGS=-I /usr/include/kea -fpic -std=c++11
LDFLAGS=-shared
LIBS=-lkea-dhcpsrv -lkea-dhcp++ -lkea-hooks -lkea-log -lkea-util -lkea-exceptions

.PHONY: all clean

all: kea-nonak.so kea-db-select-subnet.so kea-use-configured-netmask.so kea-nodecline.so kea-log-dhcp-macs.so

clean:
	rm -f *.o *.so

%.o: %.cc
	$(CXX) -c -o $@ $< $(CXXFLAGS)

kea-nonak.so: load_unload.o version.o nonak_pkt4_send.o 
	$(CXX) -o $@ $^ $(CXXFLAGS) $(LDFLAGS) $(LIBS)

kea-db-select-subnet.so: load_unload.o version.o db_subnet4_select.o
	$(CXX) -o $@ $^ $(CXXFLAGS) $(LDFLAGS) $(LIBS)

kea-use-configured-netmask.so: load_unload.o version.o netmask_pkt4_send.o
	$(CXX) -o $@ $^ $(CXXFLAGS) $(LDFLAGS) $(LIBS)

kea-nodecline.so: load_unload.o version.o nodecline_pkt4_receive.o
	$(CXX) -o $@ $^ $(CXXFLAGS) $(LDFLAGS) $(LIBS)

kea-log-dhcp-macs.so: log_dhcp_macs_load_unload.o version.o log_dhcp_macs_pkt4_receive.o
	$(CXX) -o $@ $^ $(CXXFLAGS) $(LDFLAGS) $(LIBS)
