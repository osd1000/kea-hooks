// pkt4_receive.cc
#include "log_dhcp_macs.h"
#include <hooks/hooks.h>
#include <dhcp/pkt4.h>
#include <string>

using namespace isc::dhcp;
using namespace isc::hooks;
using namespace std;
extern "C" {

// This callout is called at the "pkt4_receive" hook.
  int pkt4_receive(CalloutHandle& handle) {
    Pkt4Ptr query4_ptr;
    HWAddrPtr hwaddr_ptr;
    handle.getArgument("query4", query4_ptr);
    hwaddr_ptr = query4_ptr->getHWAddr();

    macslog << std::time(0) << "," << hwaddr_ptr->toText(false) << "\n";
    flush(macslog);

    return (0);
  };
}
