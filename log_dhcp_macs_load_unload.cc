// load_unload.cc
#include <hooks/hooks.h>
#include "log_dhcp_macs.h"

using namespace isc::hooks;

std::fstream macslog;

extern "C" {
int load(LibraryHandle&) {
  macslog.open("/var/log/kea/mac-addresses.log",
	       std::fstream::out | std::fstream::app);
  return (macslog ? 0 : 1);
}
int unload() {
  if (macslog) {
    macslog.close();
  }
  return (0);
}
}
