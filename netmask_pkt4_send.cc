// pkt4_send.cc

#include <hooks/hooks.h>
#include <dhcp/pkt4.h>
#include <dhcpsrv/cfg_option.h>
#include <dhcpsrv/subnet.h>
#include <dhcpsrv/base_host_data_source.h>
#include <dhcpsrv/host_mgr.h>
#include <iostream>

// Just before we're about to send a packet, nobble the subnet mask
// (option 1) with the correct version of this option from the
// database; kea's standard behaviour is to compute it from the subnet
// definition which in our case is not what we wanted.

using namespace isc::dhcp;
using namespace isc::hooks;
using namespace std;
extern "C" {

// This callout is called at the "pkt4_send" hook.
int pkt4_send(CalloutHandle& handle) {
  Pkt4Ptr response4_ptr;
  const Host::IdentifierType &idtype_ref = Host::IDENT_HWADDR;
    HWAddrPtr hwaddr_ptr;
  ConstHostCollection chs;
  ConstHostPtr f;
  int subnet_id;
  HostDataSourcePtr hds_ptr;
  ConstCfgOptionPtr cfgoption4_ptr;
  OptionPtr pkt_option;

  handle.getArgument("response4", response4_ptr);
  
  hwaddr_ptr=response4_ptr->getHWAddr();
  pkt_option=response4_ptr->getOption(1);

  if (pkt_option) {

    hds_ptr=HostMgr::instance().getHostDataSource();

    chs=hds_ptr->getAll(idtype_ref, hwaddr_ptr->hwaddr_.data(), 6);
  
    if (chs.size() > 0) {
      f=chs.front();

      cfgoption4_ptr=f->getCfgOption4();

      OptionDescriptor optdesc=cfgoption4_ptr->get("dhcp4",1);

      if (optdesc.option_) {
	response4_ptr->delOption(1);
	response4_ptr->addOption(optdesc.option_);
      }
    }
  }
  return (0);
};
}
