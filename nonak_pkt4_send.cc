// pkt4_send.cc
#include <hooks/hooks.h>
#include <dhcp/pkt4.h>

using namespace isc::dhcp;
using namespace isc::hooks;
using namespace std;
extern "C" {

// This callout is called at the "pkt4_send" hook.
int pkt4_send(CalloutHandle& handle) {
  Pkt4Ptr response4_ptr;
  handle.getArgument("response4", response4_ptr);
  if (response4_ptr->getType() == isc::dhcp::DHCPNAK) {
    // it's a NAK, drop it on the floor
    handle.setStatus(CalloutHandle::NEXT_STEP_DROP);
  }
  return (0);
};
}
